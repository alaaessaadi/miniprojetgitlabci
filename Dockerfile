FROM nginx:alpine
LABEL maintainer="ALAA <ALAA@gmail.com>"
RUN apk update
RUN apk --no-cache add zip unzip curl
COPY static-website-example.zip /tmp
RUN unzip /tmp/static-website-example.zip
# copy Nginx config files
COPY default.conf /etc/nginx/conf.d/
COPY nginx.conf /etc/nginx/
# set file permissions for nginx user
RUN chown -R nginx:nginx /var/cache/nginx /etc/nginx/
RUN cp -Rf static-website-example/* /usr/share/nginx/html
# switch to non-root user
USER nginx
CMD ["nginx", "-g", "daemon off;"]
